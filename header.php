<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package unite
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  
  <?php wp_head(); ?>

  <!-- Piwik -->
  <script type="text/javascript">
    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="//bank.getgoing.nl/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '5']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <!-- End Piwik Code -->
  <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
  <style type="text/css">
    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
  </style>

</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
<header class="site-header navbar">
  <div class="hidden-xs navbar-topbar">
    <div class="container-fluid">
      <div class="row">
        <div class="menu col-xs-6 col-sm-12">
          <ul>
          <li>
            <i class="fa fa-envelope-o"></i>
            <a href="mailto:<?php echo of_get_option( 'sub_email', 'info@dykhoeve.nl' ); ?>">
              <?php echo of_get_option( 'sub_email', 'info@dykhoeve.nl' ); ?>
            </a>
          </li>
          <li>
            <i class="fa fa-facebook-official"></i> <a href="https://facebook.com/<?php echo of_get_option( 'text_facebook_id', 'Dykhoeve' ); ?>"><?php echo of_get_option( 'text_facebook_id', 'Dykhoeve' ); ?></a>
          </li>
          <li class="hidden-sm">
            <i class="glyphicon glyphicon-ok-circle"></i> <?php echo of_get_option( 'text_subtitle2', 'Subtitle 2' ); ?>
          </li>
          </ul>
          <div id="navbar-search-topbar" class="hidden-xs pull-right widget_products_predictive_search">
            <div class="wc_ps_container wc_ps_sidebar_container" id="wc_ps_container_2">
              <form class="wc_ps_form" id="wc_ps_form_1" autocomplete="off" action="//test2.dykhoeve.nl/woocommerce-search/" method="get" data-ps-id="1" data-ps-cat_align="left" data-ps-cat_max_wide="30" data-ps-popup_wide="input_wide" data-ps-widget_template="sidebar">
                <input class="wc_ps_category_selector" name="cat_in" value="" type="hidden">
                <div class="wc_ps_nav_right">
                  <div class="wc_ps_nav_submit">
                    <i class="fa fa-search wc_ps_nav_submit_icon" aria-hidden="true"></i>
                    <input data-ps-id="1" class="wc_ps_nav_submit_bt" value="Go" type="button">
                  </div>
                </div>
                <div class="wc_ps_nav_fill">
                  <div class="wc_ps_nav_field">
                    <input name="rs" class="wc_ps_search_keyword ac_input_1" id="wc_ps_search_keyword_1" onblur="if( this.value == '' ){ this.value = ''; }" onfocus="if( this.value == '' ){ this.value = ''; }" value="" data-ps-id="1" data-ps-default_text="" data-ps-row="12" data-ps-text_lenght="100" data-ps-popup_search_in="{&quot;product&quot;:&quot;6&quot;,&quot;page&quot;:&quot;2&quot;,&quot;post&quot;:&quot;1&quot;,&quot;p_sku&quot;:&quot;0&quot;,&quot;p_cat&quot;:&quot;0&quot;,&quot;p_tag&quot;:&quot;0&quot;}" data-ps-search_in="product" data-ps-search_other="product,page,post" data-ps-show_price="1" data-ps-show_in_cat="1" autocomplete="off" type="text">
                    <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw wc_ps_searching_icon" style="display: none;"></i>
                  </div>
                </div>
                <input name="search_in" value="product" type="hidden">
                <input name="search_other" value="product,page,post" type="hidden">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="navbar-header">
    <div class="container">
      <?php if( get_header_image() != '' ) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand"><img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php bloginfo( 'name' ); ?>" class="img-responsive"></a>
      <?php endif; // header image was removed ?>
      <?php if( !get_header_image() ) : ?>
      <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
      <h4 class="site-description"><?php bloginfo( 'description' ); ?></h4>
      <?php endif; // header image was removed (again) ?>
      <a class="customcart btn navbar-cart" href="/cart/">
        <i class="visible-xs fa fa-shopping-cart">
          <span class="visible-xs amount badge badge-primary">
            <span class="count"><?php echo woo_count(); ?></span>
          </span>
        </i>
        <i class="hidden-xs fa fa-shopping-cart">
          <span class="hidden-xs amount badge badge-primary">
            <span class="count"><?php echo woo_count(); ?></span>
          </span>
        </i>
        <span class="total hidden-xs"><?php echo woo_total(); ?></span>
        <span class="text hidden-xs">Cart</span> <i class="fa fa-angle-right hidden-xs"></i>
      </a>
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			  <i class="fa fa-bars"></i>
			</button>
      <nav class="navbar-collapse hidden-xs hidden-sm hidden-md">
        <?php
            wp_nav_menu( array(
              'theme_location'    => 'primary',
              'depth'             => 2,
              'container'         => false,
              'menu_class'        => 'nav navbar-nav',
              'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
              'walker'            => new wp_bootstrap_navwalker())
            );
        ?>
      </nav>
      <button type="button" class="navbar-search-toggle collapsed visible-xs" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false" aria-controls="navbar-search">
        <i class="fa fa-search"></i>
      </button>
			<div id="navbar-search" class="widget widget_products_predictive_search collapse hidden-sm hidden-md hidden-lg hidden-xl">
				<div class="wc_ps_container wc_ps_sidebar_container" id="wc_ps_container_3">
					<form class="wc_ps_form" id="wc_ps_form_2" autocomplete="off" action="//test2.dykhoeve.nl/woocommerce-search/" method="get" data-ps-id="2" data-ps-cat_align="left" data-ps-cat_max_wide="30" data-ps-popup_wide="input_wide" data-ps-widget_template="sidebar">
						<input class="wc_ps_category_selector" name="cat_in" value="" type="hidden">
						<div class="wc_ps_nav_right">
							<div class="wc_ps_nav_submit">
								<i class="fa fa-search wc_ps_nav_submit_icon" aria-hidden="true"></i>
								<input data-ps-id="2" class="wc_ps_nav_submit_bt" value="Go" type="button">
							</div>
						</div>
						<div class="wc_ps_nav_fill">
							<div class="wc_ps_nav_field">
								<input name="rs" class="wc_ps_search_keyword ac_input_2" id="wc_ps_search_keyword_2" onblur="if( this.value == '' ){ this.value = ''; }" onfocus="if( this.value == '' ){ this.value = ''; }" value="" data-ps-id="2" data-ps-default_text="" data-ps-row="12" data-ps-text_lenght="100" data-ps-popup_search_in="{&quot;product&quot;:&quot;6&quot;,&quot;page&quot;:&quot;2&quot;,&quot;post&quot;:&quot;1&quot;,&quot;p_sku&quot;:&quot;0&quot;,&quot;p_cat&quot;:&quot;0&quot;,&quot;p_tag&quot;:&quot;0&quot;}" data-ps-search_in="product" data-ps-search_other="product,page,post" data-ps-show_price="1" data-ps-show_in_cat="1" autocomplete="off" type="text">
								<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw wc_ps_searching_icon" style="display: none;"></i>
							</div>
						</div>
						<input name="search_in" value="product" type="hidden">
						<input name="search_other" value="product,page,post" type="hidden">
					</form>
				</div>
			</div>
    </div>
  </div>
  <div class="hidden-lg hidden-xl nav-bar-md">
    <nav id="navbar" class="navbar-collapse collapse">
      <div class="container">
        <?php
            wp_nav_menu( array(
                'theme_location'    => 'primary',
                'depth'             => 2,
                'container'         => false,
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>
      </div>
    </nav>
  </div>
</header>

<div id="content" class="site-content container"><?php
            global $post;
            if( is_singular() && get_post_meta($post->ID, 'site_layout', true) ){
                $layout_class = get_post_meta($post->ID, 'site_layout', true);
            }
            else{
                $layout_class = of_get_option( 'site_layout' );
            }
            ?>
            <div class="row <?php echo $layout_class; ?>">
